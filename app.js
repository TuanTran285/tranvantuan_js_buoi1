// bài1: tính tiền lương nhân viên

// sơ đồ 3 khối
// input : tiền lương, số ngầy làm
// giải thuật: lấy tiền lương nhân với số ngày làm việc
// output : tổng tiền lương

// code
const salary = 100000
const workingDay = 10

const total = salary * workingDay
console.log(accounting.formatNumber(total))


// bài 2 tính giá trị trung bình

// input: 5 số thực
// giải thuật: lấy 5 số cộng lại chia cho 5 => output
// output: giá trị trung bình của 5 số


const inputs = document.querySelectorAll("#input")
const btn = document.querySelector("button")
let text = document.querySelector(".text")

function CheckInput() {
    let result = 0
    inputs.forEach(input => {
        const inputValue = input.value
        if(inputValue == "") {
            text.classList.add("invalid")
            text.innerText = "Vui lòng nhập đầy đủ thông tin"
        }else {
            text.classList.remove("invalid")
            text.classList.add("valid")
            result += inputValue / inputs.length
            text.innerText = result.toFixed(2)
        }
    })
}


btn.addEventListener("click", function(e) {
    CheckInput()
    
})



// bai3

// input: nhập số tiền muốn quy đổi, giá trị mặc định quy đổi là 23.500vnd 
// giải thuật: lấy số tiền nhập vào * số tiền quy đổi mặc định sang vnd 23.500vnd
// output: xuất ra được số tiền quy đổi từ usd sang vnd

// code
const vnd = 23500
const userInput = 2

const result = userInput * vnd

console.log(accounting.formatNumber(result))



// bài4: tính diện tích chu vi hình chữ nhật

// input: chiều dài, chiều rộng HCN
// giải thuật: 
// chu vi = dài + rộng
// diện tích = (dài + rộng) * 2 
// output: chu vi, diện tích

const cDai = 5
const cRong = 3

const chuVi = cDai + cRong
const dienTich = (cDai + cRong) * 2
console.log(chuVi) 
console.log(dienTich)


// bài5: tính tổng 2 ký số

let num1 = 12

let donVi = num1 % 10
let chuc = num1 / 10

let tong2KySo = Math.floor(donVi + chuc)
console.log(tong2KySo)



